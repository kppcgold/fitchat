package android.myproject.trams.fitchat.adapter;

/*
 * Create by FitChat on 2018. 9. 18.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

import android.myproject.trams.fitchat.main.viewpage.Tabfragment1;
import android.myproject.trams.fitchat.main.viewpage.Tabfragment2;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int tabCount;

    public PagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Tabfragment1 tabFragment1 = new Tabfragment1();
                return tabFragment1;
            case 1:
                Tabfragment2 tabFragment2 = new Tabfragment2();
                return tabFragment2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}

