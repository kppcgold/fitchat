package android.myproject.trams.fitchat.adapter;


import android.annotation.SuppressLint;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.utils.Utils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ChatAdapter extends BaseAdapter {


    public class ListContents {
        String msg;
        String regDate;
        int type;

        ListContents(String _msg, String _regDate, int _type) {
            this.msg = _msg;
            this.regDate = _regDate;
            this.type = _type;
        }
    }

    private ArrayList<ListContents> m_List;

    public ChatAdapter() {
        m_List = new ArrayList<ListContents>();
    }

    // 외부에서 아이템 추가 요청 시 사용
    public void add(String _msg, String _regDate, int _type) {
        m_List.add(new ListContents(_msg, _regDate, _type));
    }

    // 외부에서 아이템 삭제 요청 시 사용
    public void remove(int _position) {
        m_List.remove(_position);
    }

    @Override
    public int getCount() {
        return m_List.size();
    }

    @Override
    public Object getItem(int position) {
        return m_List.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ResourceType")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        TextView text = null;
        CustomHolder holder = null;
        LinearLayout layout = null;
        LinearLayout pLayout = null;
        View viewRight = null;
        View viewLeft = null;
        TextView regDate = null;

        // 리스트가 길어지면서 현재 화면에 보이지 않는 아이템은 converView가 null인 상태로 들어 옴
        if (convertView == null) {
            // view가 null일 경우 커스텀 레이아웃을 얻어 옴
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_chat, parent, false);

            layout = (LinearLayout) convertView.findViewById(R.id.layout);
            pLayout = (LinearLayout) convertView.findViewById(R.id.pLayout);
            text = (TextView) convertView.findViewById(R.id.text);
            viewRight = (View) convertView.findViewById(R.id.imageViewright);
            viewLeft = (View) convertView.findViewById(R.id.imageViewleft);
            regDate = (TextView) convertView.findViewById(R.id.regDate);

            // 홀더 생성 및 Tag로 등록
            holder = new CustomHolder();
            holder.m_TextView = text;
            holder.layout = layout;
            holder.pLayout = pLayout;
            holder.viewRight = viewRight;
            holder.viewLeft = viewLeft;
            holder.regDate = regDate;
            convertView.setTag(holder);
        } else {
            holder = (CustomHolder) convertView.getTag();
            text = holder.m_TextView;
            layout = holder.layout;
            pLayout = holder.pLayout;
            viewRight = holder.viewRight;
            viewLeft = holder.viewLeft;
            regDate = holder.regDate;
        }
        // Text 등록


        text.setText(m_List.get(position).msg);

        if (!m_List.get(position).regDate.isEmpty()) {
            regDate.setText(Utils.getInst().getTimeExpression(new Date(Long.parseLong(m_List.get(position).regDate))));
        } else {
            regDate.setText("");

        }


        if (m_List.get(position).type == 0) {
            text.setBackgroundResource(R.drawable.bubble_white);
            text.setPadding(15, 10, 15, 10);
            text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            text.setGravity(Gravity.LEFT);
            text.setTextColor(Color.rgb(0, 0, 0));
            text.setLinkTextColor(Color.rgb(0, 0, 0));

            regDate.setPadding(15, 10, 15, 10);
            regDate.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            pLayout.setGravity(Gravity.LEFT);

            layout.setGravity(Gravity.LEFT);

            regDate.setVisibility(View.VISIBLE);
            viewRight.setVisibility(View.GONE);
            viewLeft.setVisibility(View.GONE);

        } else if (m_List.get(position).type == 1) {
            text.setBackgroundResource(R.drawable.bubble_purple);
            text.setPadding(15, 10, 15, 10);
            text.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            text.setTextColor(Color.rgb(255, 255, 255));
            text.setLinkTextColor(Color.rgb(255, 255, 255));
            text.setGravity(Gravity.RIGHT);

            regDate.setPadding(15, 10, 15, 10);
            regDate.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            pLayout.setGravity(Gravity.RIGHT);

            layout.setGravity(Gravity.RIGHT);

            regDate.setVisibility(View.VISIBLE);
            viewRight.setVisibility(View.GONE);
            viewLeft.setVisibility(View.GONE);
        } else if (m_List.get(position).type == 2) {

            text.setText(Utils.getInst().convertMillisecond(m_List.get(position).regDate));
            text.setBackgroundResource(0);
            text.setTextColor(Color.parseColor("#999999"));
            regDate.setVisibility(View.GONE);
            viewRight.setVisibility(View.VISIBLE);
            viewLeft.setVisibility(View.VISIBLE);


        } else {
            text.setVisibility(View.GONE);
        }
//        else if (m_List.get(position).type == 2) {
//            text.setBackgroundResource(R.drawable.datebg);
//            layout.setGravity(Gravity.CENTER);
//            viewRight.setVisibility(View.VISIBLE);
//            viewLeft.setVisibility(View.VISIBLE);
//        }

        // 리스트 아이템을 터치 했을 때 이벤트 발생
        final TextView finalText = text;
        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // 터치 시 해당 아이템 이름 출력
                Toast.makeText(context, "리스트 클릭 : " + m_List.get(pos), Toast.LENGTH_SHORT).show();
            }
        });

        // 리스트 아이템을 길게 터치 했을때 이벤트 발생
        convertView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                // 터치 시 해당 아이템 이름 출력
                String mCopyText = finalText.getText().toString();
                Toast.makeText(context, "리스트 롱 클릭 : " + m_List.get(pos), Toast.LENGTH_SHORT).show();

                return true;
            }
        });

        return convertView;
    }

    private class CustomHolder {
        TextView m_TextView;
        LinearLayout layout;
        LinearLayout pLayout;
        View viewRight;
        View viewLeft;
        TextView regDate;
    }
}

