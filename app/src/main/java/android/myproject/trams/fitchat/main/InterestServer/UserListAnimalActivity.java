/*
 * Create by FitChat on 2018. 11. 5.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.main.InterestServer;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.usb.UsbRequest;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.myproject.trams.fitchat.adapter.ListViewAdapter;
import android.myproject.trams.fitchat.chat.ChatActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class UserListAnimalActivity extends AppCompatActivity {
    AlertDialog.Builder al;
    ListView mUsersList = null;
    TextView mNoUsersText, mAnimalBar;
    ListViewAdapter lvAdpater;
    int totalUsers = 0;
    ProgressDialog pd;
    Firebase mReference3, mReference5;
    
    String url1 = "https://fitchat-2dc3f.firebaseio.com/users/";
    String url2 = UserDetails.cNickname;
    String url3 = "/interest";
    final String re = url1 + url2 + url3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list_animal);
        lvAdpater = new ListViewAdapter();
        mUsersList = (ListView) findViewById(R.id.usersList);
        mNoUsersText = (TextView) findViewById(R.id.nouserstext);
        mAnimalBar = (TextView) findViewById(R.id.cToolbar);
        mAnimalBar.setText(R.string.iAnimal);
        mAnimalBar.setTextSize(15);
//프로그래서 다이얼로그 실행문
        pd = new ProgressDialog(UserListAnimalActivity.this);
        pd.setMessage("Connecting...");
        pd.show();
        Log.v("ChatSystem", "사용자 이름 " + UserDetails.cNickname);
        Log.v("ChatSystem", "상대방 이름 " + UserDetails.cChatWith);
        Firebase.setAndroidContext(this);

        String url = "https://fitchat-2dc3f.firebaseio.com/users.json";
        

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                doOnSuccess(s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("" + error);
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(UserListAnimalActivity.this);
        rQueue.add(request);

        mUsersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (UserDetails.cChatWith == null) {
                    al.setTitle("Error").setMessage("상대방의 이름을 못불러오고 있습니다. \n개발에게 문의 하세요").setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }else {
                    UserDetails.cChatWith = lvAdpater.get(position);
                    startActivity(new Intent(UserListAnimalActivity.this, ChatActivity.class));
                }
                Log.v("ChatSystem", "선택된 상대 " + UserDetails.cChatWith);
                Log.v("ChatSystem", "상대방 아이디 " + lvAdpater.get(position));
            }
        });
    }

    public void doOnSuccess(String s) {
        mReference3 = new Firebase("https://fitchat-2dc3f.firebaseio.com/users"+ UserDetails.cNickname + "/interest");
        try {

            JSONObject obj = new JSONObject(s);
            Iterator i = obj.keys();
            String key = "";

            while (i.hasNext()) {
                key = i.next().toString();
                mReference5 = new Firebase("https://fitchat-2dc3f.firebaseio.com/users/");

                String user = obj.getJSONObject(key).getString("interest");

                Log.v("InterestSystem", "상대방 관심사 = " + user);
                Log.v("InterestSystem", "상대방 관심사 = " + user.equals("animal"));
                Log.v("UserServer", "interest = " + UserDetails.cInterest);



                if (!key.equals(UserDetails.cNickname)) {
                    if (user.equals("animal")) {
                        lvAdpater.add(key, "", null, "");
                    }
                }
                String yourI = url1 + key + url3;
                Log.v("InterestSystem", "상대방 url = " + mReference5);
                totalUsers++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (totalUsers <= 1) {
            mNoUsersText.setVisibility(View.VISIBLE);
            mUsersList.setVisibility(View.GONE);
        } else {
            mNoUsersText.setVisibility(View.GONE);
            mUsersList.setVisibility(View.VISIBLE);
            mUsersList.setAdapter(lvAdpater);
        }

        pd.dismiss();
    }

}
