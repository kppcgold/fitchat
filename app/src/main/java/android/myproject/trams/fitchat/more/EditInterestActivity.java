package android.myproject.trams.fitchat.more;

import android.app.Application;
import android.content.Intent;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;

import android.myproject.trams.fitchat.main.MainActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.firebase.client.Firebase;

import java.util.HashMap;
import java.util.Map;

public class EditInterestActivity extends AppCompatActivity {

    RelativeLayout eiAnimal, eiArt, eiBeauty, eiCar, eiCartoon, eiCooking, eiDance, eiFashion, eiGame, eiInterior, eiMusic, eiMovie, eiSports, eiTrip;
    ImageButton eiAnimalBtn, eiArtBtn, eiBeautyBtn, eiCarBtn, eiCartoonBtn, eiCookingBtn, eiDanceBtn, eiFashionBtn, eiGameBtn, eiInteriorBtn, eiMusicBtn, eiMovieBtn, eiSportsBtn, eiTripBtn;
    Firebase mReference4, mReference5;
    String mAnimal, mArt, mBeauty, mCar, mCartoon, mCooking, mDance, mFashion, mGame, mInterior, mMusic, mMovie, mSports, mTrip, mUser, mYour;
    TextView mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_interest);
        mToolbar = (TextView) findViewById(R.id.cToolbar);
        mToolbar.setText("Please choose a interest.");
        mToolbar.setTextSize(16);
        mToolbar.setGravity(Gravity.CENTER);
        eiAnimalBtn = (ImageButton) findViewById(R.id.eianimal_btn);
        eiArtBtn = (ImageButton) findViewById(R.id.eiArt);
        eiBeautyBtn = (ImageButton) findViewById(R.id.eibeauty);
        eiCarBtn = (ImageButton) findViewById(R.id.eicar);
        eiCartoonBtn = (ImageButton) findViewById(R.id.eicartoon);
        eiCookingBtn = (ImageButton) findViewById(R.id.eicooking);
        eiDanceBtn = (ImageButton) findViewById(R.id.eidance);
        eiFashionBtn = (ImageButton) findViewById(R.id.eifashion);
        eiGameBtn = (ImageButton) findViewById(R.id.eigame);
        eiInteriorBtn = (ImageButton) findViewById(R.id.eiinterior);
        eiMusicBtn = (ImageButton) findViewById(R.id.eimusic);
        eiMovieBtn = (ImageButton) findViewById(R.id.eimovie);
        eiSportsBtn = (ImageButton) findViewById(R.id.eisports);
        eiTripBtn = (ImageButton) findViewById(R.id.eitrip);

        final String url = "https://fitchat-2dc3f.firebaseio.com/users.json";
        mUser = UserDetails.cNickname;

        Log.v("EditItSystem", "상대관심사1는 = " + UserDetails.cInterest);
        mReference4 = new Firebase("https://fitchat-2dc3f.firebaseio.com/users/" + mUser + "/interest");
        mReference5 = new Firebase("https://fitchat-2dc3f.firebaseio.com/users/" + mYour + "/interest");
        Firebase.setAndroidContext(this);
        final Map<String, String> interestmap = new HashMap<String, String>();
//        interestmap.put("interest", );
        mYour = interestmap.get("interest");
        UserDetails.cInterest = interestmap.get("interest");
        Log.v("EditItSystem", "상대 관심사는 = " + interestmap.get("interest"));
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.eianimal_btn:
                        mAnimal = "animal";
                        mReference4.setValue(mAnimal);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : animal", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eiArt:
                        mArt = "art";
                        mReference4.setValue(mArt);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : art", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eibeauty:
                        mBeauty = "beauty";
                        mReference4.setValue(mBeauty);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : beauty", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eicar:
                        mCar = "car";
                        mReference4.setValue(mCar);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : car", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eicartoon:
                        mCartoon = "cartoon";
                        mReference4.setValue(mCartoon);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : cartoon", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eicooking:
                        mCooking = "cooking";
                        mReference4.setValue(mCooking);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : cooking", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eidance:
                        mDance = "dance";
                        mReference4.setValue(mDance);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : dance", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eifashion:
                        mFashion = "fashion";
                        mReference4.setValue(mFashion);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : fashion", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eigame:
                        mGame = "game";
                        mReference4.setValue(mGame);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : game", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eiinterior:
                        mInterior = "interior";
                        mReference4.setValue(mInterior);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : interior", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eimusic:
                        mMusic = "music";
                        mReference4.setValue(mMusic);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : music", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eimovie:
                        mMovie = "movie";
                        mReference4.setValue(mMovie);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : movie", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eisports:
                        mSports = "sports";
                        mReference4.setValue(mSports);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : sports", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.eitrip:
                        mTrip = "trip";
                        mReference4.setValue(mTrip);
                        startActivity(new Intent(EditInterestActivity.this, MainActivity.class));
                        finish();
                        Toast.makeText(EditInterestActivity.this, "관심사 등록 성공 : trip", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };
        eiAnimalBtn.setOnClickListener(clickListener);
        eiArtBtn.setOnClickListener(clickListener);
        eiBeautyBtn.setOnClickListener(clickListener);
        eiCarBtn.setOnClickListener(clickListener);
        eiCartoonBtn.setOnClickListener(clickListener);
        eiCookingBtn.setOnClickListener(clickListener);
        eiDanceBtn.setOnClickListener(clickListener);
        eiFashionBtn.setOnClickListener(clickListener);
        eiGameBtn.setOnClickListener(clickListener);
        eiInteriorBtn.setOnClickListener(clickListener);
        eiMusicBtn.setOnClickListener(clickListener);
        eiMovieBtn.setOnClickListener(clickListener);
        eiSportsBtn.setOnClickListener(clickListener);
        eiTripBtn.setOnClickListener(clickListener);
    }
}

