/*
 * Create by FitChat on 2018. 9. 10.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.main;

import android.content.Intent;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.main.InterestServer.UserListAnimalActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListArtActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListBeautyActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListCarActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListCartoonActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListCookingActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListDanceActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListFashionActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListGameActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListInteriorActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListMovieActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListMusicActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListSportsActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListTripActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;


public class InterestActivity extends AppCompatActivity {

    ImageButton iAnimalBtn, iArtBtn, iBeautyBtn, iCarBtn, iCartoonBtn, iCookingBtn, iDanceBtn, iFashionBtn, iGameBtn, iInteriorBtn, iMusicBtn, iMovieBtn, iSportsBtn, iTripBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interest);

        iAnimalBtn = (ImageButton) findViewById(R.id.ianimal_btn);
        iArtBtn = (ImageButton) findViewById(R.id.iArt_btn);
        iBeautyBtn = (ImageButton) findViewById(R.id.ibeauty_btn);
        iCarBtn = (ImageButton) findViewById(R.id.icar_btn);
        iCartoonBtn = (ImageButton) findViewById(R.id.icartoon_btn);
        iCookingBtn = (ImageButton) findViewById(R.id.icooking_btn);
        iDanceBtn = (ImageButton) findViewById(R.id.idance_btn);
        iFashionBtn = (ImageButton) findViewById(R.id.ifashion_btn);
        iGameBtn = (ImageButton) findViewById(R.id.igame_btn);
        iInteriorBtn = (ImageButton) findViewById(R.id.iinterior_btn);
        iMusicBtn = (ImageButton) findViewById(R.id.imusic_btn);
        iMovieBtn = (ImageButton) findViewById(R.id.imovie_btn);
        iSportsBtn = (ImageButton) findViewById(R.id.isports_btn);
        iTripBtn = (ImageButton) findViewById(R.id.itrip_btn);

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.ianimal_btn:
                        startActivity(new Intent(InterestActivity.this, UserListAnimalActivity.class));
                        break;
                    case R.id.iArt_btn:
                        startActivity(new Intent(InterestActivity.this, UserListArtActivity.class));
                        break;
                    case R.id.ibeauty_btn:
                        startActivity(new Intent(InterestActivity.this, UserListBeautyActivity.class));
                        break;
                    case R.id.icar_btn:
                        startActivity(new Intent(InterestActivity.this, UserListCarActivity.class));
                        break;
                    case R.id.icartoon_btn:
                        startActivity(new Intent(InterestActivity.this, UserListCartoonActivity.class));
                        break;
                    case R.id.icooking_btn:
                        startActivity(new Intent(InterestActivity.this, UserListCookingActivity.class));
                        break;
                    case R.id.idance_btn:
                        startActivity(new Intent(InterestActivity.this, UserListDanceActivity.class));
                        break;
                    case R.id.ifashion_btn:
                        startActivity(new Intent(InterestActivity.this, UserListFashionActivity.class));
                        break;
                    case R.id.igame_btn:
                        startActivity(new Intent(InterestActivity.this, UserListGameActivity.class));
                        break;
                    case R.id.iinterior_btn:
                        startActivity(new Intent(InterestActivity.this, UserListInteriorActivity.class));
                        break;
                    case R.id.imusic_btn:
                        startActivity(new Intent(InterestActivity.this, UserListMusicActivity.class));
                        break;
                    case R.id.imovie_btn:
                        startActivity(new Intent(InterestActivity.this, UserListMovieActivity.class));
                        break;
                    case R.id.isports_btn:
                        startActivity(new Intent(InterestActivity.this, UserListSportsActivity.class));
                        break;
                    case R.id.itrip_btn:
                        startActivity(new Intent(InterestActivity.this, UserListTripActivity.class));
                        break;
                }
            }
        };
        iAnimalBtn.setOnClickListener(clickListener);
        iArtBtn.setOnClickListener(clickListener);
        iBeautyBtn.setOnClickListener(clickListener);
        iCarBtn.setOnClickListener(clickListener);
        iCartoonBtn.setOnClickListener(clickListener);
        iCookingBtn.setOnClickListener(clickListener);
        iDanceBtn.setOnClickListener(clickListener);
        iFashionBtn.setOnClickListener(clickListener);
        iGameBtn.setOnClickListener(clickListener);
        iInteriorBtn.setOnClickListener(clickListener);
        iMusicBtn.setOnClickListener(clickListener);
        iMovieBtn.setOnClickListener(clickListener);
        iSportsBtn.setOnClickListener(clickListener);
        iTripBtn.setOnClickListener(clickListener);


    }
}