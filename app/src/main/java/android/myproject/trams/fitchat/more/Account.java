/*
 * Create by FitChat on 2018. 11. 24.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.more;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.myproject.trams.fitchat.Start.LoginActivity;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.main.viewpage.Tabfragment2;
import android.myproject.trams.fitchat.UserDetails;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.appizona.yehiahd.fastsave.FastSave;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Account extends AppCompatActivity {
    LinearLayout mLogout, mLeave;
    AlertDialog.Builder mADialog;
    Firebase firebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        mLogout = (LinearLayout) findViewById(R.id.logout);
        mLeave = (LinearLayout) findViewById(R.id.leave);
        final Tabfragment2 Tf2 = (Tabfragment2) Tabfragment2.Tabfragment2;
        mADialog = new AlertDialog.Builder(Account.this);
        mLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* 계정 로그아웃(자동로그인) */
                FastSave.getInstance().saveString("cNickname", "");
                FastSave.getInstance().saveString("cPassword", "");

                startActivity(new Intent(getApplication(), LoginActivity.class));
                Account.this.finish();
                Tf2.getActivity().finish();
            }
        });
//        final EditText et = new EditText(Account.this);
        mADialog.setView(R.layout.item_edit_text);
//  탈퇴 버튼 클릭시 경고팝업 출력
        mLeave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//          경고 팝업 내부 setCancelable = 백 버튼으로 취소 T/F설정
                mADialog.setTitle(getResources().getText(R.string.mLeave)).setMessage(getResources().getText(R.string.mLeave_msg)).setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();

                        Dialog d = (Dialog) dialog;
                        EditText input = (EditText) d.findViewById(R.id.input);
                        String value = input.getText().toString();
                        if (UserDetails.cPassword.matches(value)) {

                            /* 계정 로그아웃(자동로그인) */
                            FastSave.getInstance().saveString("cNickname", "");
                            FastSave.getInstance().saveString("cPassword", "");

                            startActivity(new Intent(getApplication(), LoginActivity.class));
                            Account.this.finish();
                            Tf2.getActivity().finish();
                            Log.e("leave", "탈퇴 성공");
                        } else {
                            Toast.makeText(Account.this, "incorrect password", Toast.LENGTH_SHORT).show();
                        }

                        Log.v("mADialog", "수락버튼 클릭");
                        Log.v("mADialogPsw", value);
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.v("mADialog", "취소버튼 클릭");
                        dialog.dismiss();
                    }
                });
                mADialog.show();
            }
        });
    }
}
