package android.myproject.trams.fitchat.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.myproject.trams.fitchat.adapter.ListViewAdapter;
import android.myproject.trams.fitchat.chat.ChatActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HistoryList extends AppCompatActivity {
    Firebase mReference3 ;
    ListView friend_list;
    ListViewAdapter lvAdapter;
    AlertDialog.Builder cADialog;
    int totalUsers = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_list);
        friend_list = (ListView) findViewById(R.id.friend_list);
        Firebase.setAndroidContext(this);
        mReference3 = new Firebase("https://fitchat-2dc3f.firebaseio.com/history/" + UserDetails.cChatWith + "_" + UserDetails.cNickname);
        lvAdapter = new ListViewAdapter();
        String url = "https://fitchat-2dc3f.firebaseio.com/history.json";

        lvAdapter = new ListViewAdapter();
        friend_list.setAdapter(lvAdapter);

        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                doOnSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print("" + error);
            }
        });
        friend_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (UserDetails.cChatWith == null) {
                    cADialog.setTitle("Error").setMessage("상대방의 이름을 못불러오고 있습니다. \n개발에게 문의 하세요").setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                } else {
                    UserDetails.cChatWith = lvAdapter.get(position);
                    startActivity(new Intent(getApplication(), ChatActivity.class));
                }
                Log.v("ChatSystem", "선택된 상대 " + UserDetails.cChatWith);
                Log.v("ChatSystem", "상대방 아이디 " + lvAdapter.get(position));
            }
        });

        RequestQueue rQueue = Volley.newRequestQueue(this);
        rQueue.add(request);
    }

    public void doOnSuccess(String s) {
        try {
            Map<String, String> map = new HashMap<String, String>();
            JSONObject obj = new JSONObject(s);
            Iterator i = obj.keys();
            String key = "";
            while (i.hasNext()) {
                key = i.next().toString();
                String user = obj.getJSONObject(key).getString("user");
                String history = obj.getJSONObject(key).getString("message");
                String regDate = obj.getJSONObject(key).isNull("regDate") ? "" : obj.getJSONObject(key).getString("regDate");

                if (!user.equals(UserDetails.cNickname)) {
                    lvAdapter.add(user, history, null, regDate);
                }
                Log.v("UserHistory", "상대 이름 = " + user);
                Log.v("UserHistory", "대화 내역 = " + history);
                Log.v("UserHistory", "대화 내역 주소 = " + mReference3.child("message").toString());
                totalUsers++;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (totalUsers <= 1) {
//            .setVisibility(View.VISIBLE);
            friend_list.setVisibility(View.GONE);
        } else {
//            mNoUsersText.setVisibility(View.GONE);
            friend_list.setVisibility(View.VISIBLE);
            friend_list.setAdapter(lvAdapter);
        }
    }
}
