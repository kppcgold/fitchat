/*
 * Create by FitChat on 2018. 11. 5.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.chat;

import android.myproject.trams.fitchat.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


public class ChatbotActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatbot);
    }
}