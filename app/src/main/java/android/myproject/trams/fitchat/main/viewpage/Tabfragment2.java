/*
 * Create by FitChat on 2018. 9. 1.
 * Copyright (c) 2018. FitChat. All rights reserved.
 */

package android.myproject.trams.fitchat.main.viewpage;

import android.content.Intent;
import android.myproject.trams.fitchat.more.Account;
import android.myproject.trams.fitchat.more.EditInterestActivity;
import android.myproject.trams.fitchat.more.Friend_add;
import android.myproject.trams.fitchat.R;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toolbar;

public class Tabfragment2 extends Fragment {
    Toolbar toolbar;
    LinearLayout mFriend, mAlarm, mEdit;
    public static Tabfragment2 Tabfragment2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_tabfragment2, container, false);
        mAlarm = (LinearLayout) v.findViewById(R.id.alermbtn);
        mEdit = (LinearLayout) v.findViewById(R.id.Revise);
        Tabfragment2 = Tabfragment2.this;
        mAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), Account.class));
            }
        });

        mEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), EditInterestActivity.class));
            }
        });
        return v;
    }

}

