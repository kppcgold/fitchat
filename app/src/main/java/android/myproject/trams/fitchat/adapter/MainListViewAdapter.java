/*
 * Create by FitChat on 2018. 11. 16.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.adapter;
//TODO 유저 목록 리스트 어뎁터

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.main.LIstViewItem;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainListViewAdapter extends BaseAdapter {

    public class MainUserListContents {
        String mUser;
        String mHistory;
        Drawable mIcon;

        MainUserListContents(String _user, String _history, Drawable _icon) {
            this.mUser = _user;
            this.mHistory = _history;
            this.mIcon = _icon;
        }
    }

    // Adapter에 추가된 데이터를 저장하기 위한 ArrayList
    private ArrayList<MainUserListContents> u_list;

    // ListViewAdapter의 생성자
    public MainListViewAdapter() {
        u_list = new ArrayList<MainUserListContents>();
    }

    // 외부에서 아이템 추가 요청 시 사용
    public void add(String _user, String _history, Drawable _icon) {
        u_list.add(new MainUserListContents(_user, _history, _icon));
    }

    public String get(int _position) {
        u_list.get(_position);
        return u_list.get(_position).mUser;
    }

    // 외부에서 아이템 삭제 요청 시 사용
    public void remove(int _position) {
        u_list.remove(_position);
    }


    @Override
    public int getCount() {
        return u_list.size();
    }

    @Override
    public Object getItem(int position) {
        return u_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LIstViewItem lIstViewItem = new LIstViewItem();
        final Context context = parent.getContext();
        CustomHolder holder = null;
        TextView mMember1 = null;
        TextView mHistory2 = null;
        ImageView mIcon3 = null;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            holder = new CustomHolder();
            convertView = inflater.inflate(R.layout.list_main, parent, false);

            holder.mMember = (TextView) convertView.findViewById(R.id.member_name);
            holder.mHistory = (TextView) convertView.findViewById(R.id.recent_history);
            holder.mIcon = (ImageView) convertView.findViewById(R.id.list_img);

            convertView.setTag(holder);
        } else {
            holder = (CustomHolder) convertView.getTag();
        }

        holder.mMember.setText(u_list.get(position).mUser);
        holder.mHistory.setText(u_list.get(position).mHistory);
//        holder.mIcon.setImageDrawable(u_list.get(position).mIcon);

        return convertView;
    }

    private class CustomHolder {
        TextView mMember;
        TextView mHistory;
        ImageView mIcon;
    }
}