package android.myproject.trams.fitchat.Start;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.myproject.trams.fitchat.main.MainActivity;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.myproject.trams.fitchat.more.EditInterestActivity;
import android.myproject.trams.fitchat.utils.Utils;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.appizona.yehiahd.fastsave.FastSave;
import com.firebase.client.Firebase;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {
    TextView mFindPwd, mRegister;
    EditText mId, mPwd;
    Button mLogin;
    String mNickName, mPassword, mInterest;
    CheckBox mAuthLogin;
    Firebase firebaseurl;
    EditInterestActivity editInterestActivity;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editInterestActivity = new EditInterestActivity();
        mId = (EditText) findViewById(R.id.getId);
        mPwd = (EditText) findViewById(R.id.getPwd);
        mLogin = (Button) findViewById(R.id.loginBtn);
//        mFindPwd = (TextView) findViewById(R.id.findPwdBtn);
        mRegister = (TextView) findViewById(R.id.startRegister);
//        mAuthLogin = (CheckBox) findViewById(R.id.autoLogin);
        Firebase.setAndroidContext(this);
        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mNickName = mId.getText().toString();
                mPassword = mPwd.getText().toString();
//              id 입력 폼에 아무것도 없을시 에러문구 출력
                if (mNickName.equals("")) {
                    mId.setError("can't be blank");
//              password 입력 폼에 아무것도 없을시 에러문구 출력
                } else if (mNickName.equals("")) {
                    mPwd.setError("can't be blank");
                } else {
//                    파이어베이스 유저 데이터베이스 주소
                    String url = "https://fitchat-2dc3f.firebaseio.com/users.json";
//                    프로그래스다이얼로그를 이 엑티비티에 출력한다.
                    final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
//                    프로그래스다이얼로그에 띄울 메시지
                    pd.setProgressStyle(getResources().getColor(R.color.colorPd));
                    pd.setMessage("Connecting...");
                    pd.show();

                    StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            if (s.equals("null")) {
//                              유저 데이터베이스에 등록되어있지 않은 유저이면 메시지 출력
                                Toast.makeText(getApplication(), "user not found", Toast.LENGTH_LONG).show();
                            } else {
                                try {
                                    JSONObject obj = new JSONObject(s);

                                    if (!obj.has(mNickName)) {
//                                       유저 데이터베이스에 등록되어있지 않은 이메일이면 메시지 출력
                                        Toast.makeText(getApplication(), "Email not found", Toast.LENGTH_LONG).show();
                                    } else if (obj.getJSONObject(mNickName).getString("password").equals(mPassword)) {
                                        firebaseurl = new Firebase("https://fitchat-2dc3f.firebaseio.com/user/" + UserDetails.cChatWith + "/interest");
//                                        이메일과 페스워드의 값이 db에 있는 값과 일치하면 메인 화면으로 이동
                                        Log.v("LoginSystem", "상대방 관심사" + firebaseurl.toString());
                                        UserDetails.cNickname = mNickName;
                                        UserDetails.cPassword = mPassword;
//                                        UserDetails.cInterest =

                                        /* 계정 저장(자동로그인) */
                                        FastSave.getInstance().saveString("cNickname", UserDetails.cNickname);
                                        FastSave.getInstance().saveString("cPassword", UserDetails.cPassword);

                                        /* 이동 분기처리 */
                                        Utils.getInst().moveInitPage(LoginActivity.this);

                                    } else {
//                                      유저 데이터베이스에 등록되어있지 않은 비밀번호면 메시지 출력
                                        Toast.makeText(getApplication(), "incorrect password", Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
//                            프로그래스 다이얼로그 종료
                            pd.dismiss();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            System.out.println("" + error);
                            pd.dismiss();
                        }

                    });

                    RequestQueue rQueue = Volley.newRequestQueue(getApplication());
                    rQueue.add(request);
                }

            }
        });
        mRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplication(), RegisterActivity.class));
            }
        });


    }
    private void getPreferences(){
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        pref.getString("1", "");
    }

    // 값 저장하기
    private void savePreferences(){
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("1", "사용");
    }

    // 값(Key Data) 삭제하기
    private void removePreferences(){
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.remove("1");
    }

    // 값(ALL Data) 삭제하기
    private void removeAllPreferences(){
        SharedPreferences pref = getSharedPreferences("pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
    }
}
