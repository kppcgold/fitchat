/*
 * Create by FitChat on 2018. 9. 10.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.main;

import android.app.Activity;
import android.myproject.trams.fitchat.R;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class ListItemActivity extends Activity{

    ImageView profile_img;
    TextView profile_user;
    TextView profile_message;
    LIstViewItem lvItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_main);

        profile_img.setImageDrawable(lvItem.getmProfile_img());
        profile_user.setText(lvItem.getmMember_name());
        profile_message.setText(lvItem.getmRecent_history());


    }

}