/*
 * Create by FitChat on 2018. 10. 29.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.Start;

import android.app.ProgressDialog;
import android.content.Intent;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.firebase.client.Firebase;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity {
    String email, user, pass, cfpass;
    EditText mEmail, mPwd, mCfpwd, mName;
    Button mRegist;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mEmail = (EditText) findViewById(R.id.getid);
        mPwd = (EditText) findViewById(R.id.getpwd);
        mCfpwd = (EditText) findViewById(R.id.getcfpwd);
        mName = (EditText) findViewById(R.id.getname);
        mRegist = (Button) findViewById(R.id.register);

        Firebase.setAndroidContext(this);

        mRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                email = mEmail.getText().toString();
                pass = mPwd.getText().toString();
                cfpass = mCfpwd.getText().toString();
                user = mName.getText().toString();
//              id
                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    mEmail.setError("Please enter email");
                } else if (mEmail.equals("")) {
                    mEmail.setError("can't be blank");
                }
//              password
                else if (pass.equals("")) {
                    mPwd.setError("can't bo blank");
                } else if (pass.length() < 8) {
                    mPwd.setError("Please enter an 8-20 english and digit");
                } else if (pass.length() > 20) {
                    mPwd.setError("Please enter an 8-20 english");
                } else if (!cfpass.matches(pass)) {
                    mCfpwd.setError("The passwords do not match");
                }
//              user
                else if (user.matches("[a-zA-Z0-9]")) {
                    mName.setError("Please enter english nick name");
                } else if (user.length() < 4) {
                    mName.setError("enter a 4-15 character english nick name");
                } else if (user.length() > 15) {
                    mName.setError("enter a 4-15 character english nick name");
                } else {
                    final ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
                    pd.setMessage("Loading...");
                    pd.show();

                    String url = "https://fitchat-2dc3f.firebaseio.com/users.json";

                    StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Firebase reference = new Firebase("https://fitchat-2dc3f.firebaseio.com/users");
                            Log.v("Register", "profile = " + reference.child(UserDetails.cNickname).child("interest"));
                            if (s.equals("null")) {
                                reference.child(user).child("password").setValue(pass);
                                reference.child(user).child("email").setValue(email);
                                Toast.makeText(RegisterActivity.this, "registration successful", Toast.LENGTH_LONG).show();
                            } else {
                                try {
                                    JSONObject obj = new JSONObject(s);

                                    if (!obj.has(user)) {
                                        reference.child(user).child("password").setValue(pass);
                                        reference.child(user).child("email").setValue(email);
                                        Toast.makeText(RegisterActivity.this, "registration successful", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(RegisterActivity.this, "username already exists", Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                            pd.dismiss();
                        }

                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            System.out.println("" + volleyError);
                            pd.dismiss();
                        }
                    });

                    RequestQueue rQueue = Volley.newRequestQueue(RegisterActivity.this);
                    rQueue.add(request);
                }
            }
        });


    }
}
