package android.myproject.trams.fitchat.common;

import android.app.Application;

import com.appizona.yehiahd.fastsave.FastSave;


public class App extends Application{


	@Override
	public void onCreate() {
		super.onCreate();

        /* Pref를 전역에서 사용하기 위함 */
        FastSave.init(getApplicationContext());

	}


}