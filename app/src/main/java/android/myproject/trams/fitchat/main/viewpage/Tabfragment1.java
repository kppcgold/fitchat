/*
 * Create by FitChat on 2018. 9. 1.
 * Copyright (c) 2018. FitChat. All rights reserved.
 */

package android.myproject.trams.fitchat.main.viewpage;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.myproject.trams.fitchat.adapter.ListViewAdapter;
import android.myproject.trams.fitchat.adapter.MainListViewAdapter;
import android.myproject.trams.fitchat.adapter.PagerAdapter;
import android.myproject.trams.fitchat.chat.ChatActivity;
import android.myproject.trams.fitchat.main.InterestActivity;
import android.myproject.trams.fitchat.main.InterestServer.UserListAnimalActivity;
import android.myproject.trams.fitchat.main.MainActivity;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.solver.widgets.Snapshot;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.EventLog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.fasterxml.jackson.databind.deser.DataFormatReaders;
import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.EventListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Tabfragment1 extends Fragment {
    FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
    int totalUsers = 0;
    TextView textView;
    TextView fUserName;
    AlertDialog.Builder cADialog;
    Toolbar toolbar;
    ListView friend_list;
    ListViewAdapter lvAdapter;
    ImageButton mInterest, mChatbot;
    Firebase mReference3;
    String url = "https://fitchat-2dc3f.firebaseio.com/history.json";

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.activity_tabfragment1, container, false);
        friend_list = (ListView) v.findViewById(R.id.friend_list);
        mInterest = (ImageButton) v.findViewById(R.id.interest);
        mChatbot = (ImageButton) v.findViewById(R.id.chatbot);
        fUserName = (TextView) v.findViewById(R.id.fUserName);
        textView = (TextView) v.findViewById(R.id.reload);
        cADialog = new AlertDialog.Builder(v.getContext());
        Firebase.setAndroidContext(v.getContext());

        mReference3 = new Firebase("https://fitchat-2dc3f.firebaseio.com/history/" + UserDetails.cChatWith + "_" + UserDetails.cNickname);

//      사용자의 이름을 UserDetails 에서 가져와 TextView 에 입력한다.
        fUserName.setText(UserDetails.cNickname);
//      관심사 선택 페이지로 이동한다.
        mInterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), InterestActivity.class));
            }
        });

//      ChatBot 채팅 페이지로 이동한다.
        mChatbot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cADialog.setTitle("Preparing").setMessage("Chatbot is currently being developed.\nWe will you with better service.\n\nThank you for using FitChat").setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                cADialog.show();
//                startActivity(new Intent(getContext(), ChatbotActivity.class));
            }
        });
        lvAdapter = new ListViewAdapter();
        friend_list.setAdapter(lvAdapter);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), MainActivity.class));
            }
        });

//      유저 리스트의 아이템을 클릭시 실행되는 이벤트
        friend_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (UserDetails.cChatWith == null) {
                    cADialog.setTitle("Error").setMessage("상대방의 이름을 못불러오고 있습니다. \n개발에게 문의 하세요").setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                } else {
                    UserDetails.cChatWith = lvAdapter.get(position);
                    startActivity(new Intent(getContext(), ChatActivity.class));
                }
                Log.v("ChatSystem", "선택된 상대 " + UserDetails.cChatWith);
                Log.v("ChatSystem", "상대방 아이디 " + lvAdapter.get(position));
            }
        });

        return v;
    }


    public void onResume() {
        StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                doOnSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.print("" + error);
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(request);
        super.onResume();
    }

    public void doOnSuccess(String s) {
        try {
            Map<String, String> map = new HashMap<String, String>();
            JSONObject obj = new JSONObject(s);
            Iterator i = obj.keys();
            String key = "";
            if (lvAdapter.getU_list() != null && lvAdapter.getU_list().size() > 0) {
                lvAdapter.getU_list().clear();
            }

            while (i.hasNext()) {
                key = i.next().toString();
                String user = obj.getJSONObject(key).getString("user");
                String with = obj.getJSONObject(key).getString("with");
                String history = obj.getJSONObject(key).getString("message");
                String regDate = obj.getJSONObject(key).isNull("regDate") ? "" : obj.getJSONObject(key).getString("regDate");

                    if (with.equals(UserDetails.cNickname)) {
                        lvAdapter.add(user, history, null, regDate);
                    }

                Log.v("UserHistory", "내 이름 = " + user);
                Log.v("UserHistory", "상대방 이름 = " + with);
                Log.v("UserHistory", "대화 내역 = " + history);
                Log.v("UserHistory", "대화 내역 주소 = " + mReference3.child("message").toString());

                totalUsers++;
            }

            lvAdapter.notifyDataSetChanged();

        } catch (
                JSONException e)

        {
            e.printStackTrace();
        }
        if (totalUsers <= 1)

        {
//            .setVisibility(View.VISIBLE);
            friend_list.setVisibility(View.GONE);
        } else

        {
//            mNoUsersText.setVisibility(View.GONE);
            friend_list.setVisibility(View.VISIBLE);
            friend_list.setAdapter(lvAdapter);
        }
    }

}
