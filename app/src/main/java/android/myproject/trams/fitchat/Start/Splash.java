/*
 * Create by FitChat on 2018. 8. 28.
 * Copyright (c) 2018. FitChat. All rights reserved.
 */
package android.myproject.trams.fitchat.Start;

import android.app.Activity;
import android.content.Intent;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.myproject.trams.fitchat.utils.Utils;
import android.os.Bundle;
import android.os.Handler;

import com.appizona.yehiahd.fastsave.FastSave;

public class Splash extends Activity {

    /** 로딩 화면이 떠있는 시간(밀리초단위)  **/
    private final int SPLASH_DISPLAY_LENGTH = 2500;

    /** 처음 액티비티가 생성될때 불려진다. */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_splash);

        /** SPLASH_DISPLAY_LENGTH 뒤에 메뉴 액티비티를 실행시키고 종료한다. **/
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                /* 자동로그인 체크 */
                String cNickname = FastSave.getInstance().getString("cNickname", "");
                String cPassword = FastSave.getInstance().getString("cPassword", "");


                if(!cNickname.isEmpty() && !cPassword.isEmpty()){
                    UserDetails.cNickname = cNickname;
                    UserDetails.cPassword = cPassword;

                    Utils.getInst().moveInitPage(Splash.this);
                }else{
                    /** 메뉴액티비티를 실행하고 로딩화면을 죽인다. **/
                    Intent mainIntent = new Intent(Splash.this,LoginActivity.class);
                    Splash.this.startActivity(mainIntent);
                    Splash.this.finish();
                }


            }
        }, SPLASH_DISPLAY_LENGTH);
    }
}