/*
 * Create by FitChat on 2018. 11. 5.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

package android.myproject.trams.fitchat.chat;

import android.content.Context;

import android.content.Intent;
import android.myproject.trams.fitchat.R;
import android.myproject.trams.fitchat.UserDetails;
import android.myproject.trams.fitchat.adapter.ChatAdapter;
import android.myproject.trams.fitchat.adapter.ListViewAdapter;
import android.myproject.trams.fitchat.main.viewpage.Tabfragment1;
import android.myproject.trams.fitchat.utils.Utils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

import java.util.HashMap;
import java.util.Map;

public class ChatActivity extends AppCompatActivity {
    private String prevDate = "";
    Button mSendButton;
    EditText mMessageArea;
    TextView mToolbar;
    Firebase mReference1, mReference2, mReference3, mReference4;
    ListView mMsgArea;
    ChatAdapter cAdapter;
    ListViewAdapter lvAdapter;
    public static Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        lvAdapter = new ListViewAdapter();
        mSendButton = (Button) findViewById(R.id.sendButton);
        mMessageArea = (EditText) findViewById(R.id.messageArea);
        mMsgArea = (ListView) findViewById(R.id.msgArea);
        mToolbar = (TextView) findViewById(R.id.cToolbar);

        cAdapter = new ChatAdapter();

        mContext = this;
        mMsgArea.setAdapter(cAdapter);

        Firebase.setAndroidContext(this);
        mReference1 = new Firebase("https://fitchat-2dc3f.firebaseio.com/messages/" + UserDetails.cNickname + "_" + UserDetails.cChatWith);
        mReference2 = new Firebase("https://fitchat-2dc3f.firebaseio.com/messages/" + UserDetails.cChatWith + "_" + UserDetails.cNickname);
        mReference3 = new Firebase("https://fitchat-2dc3f.firebaseio.com/history/" + UserDetails.cNickname + "_" + UserDetails.cChatWith);
        mReference4 = new Firebase("https://fitchat-2dc3f.firebaseio.com/history/" + UserDetails.cChatWith + "_" + UserDetails.cNickname);
        //엑션바에 상대방 이름 출력
        mToolbar.setText(UserDetails.cChatWith);
        final Map<String, String> map = new HashMap<String, String>();
        final HashMap<String, String> map1 = new HashMap<String, String>();
        final HashMap<String, String> map2 = new HashMap<String, String>();

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = mMessageArea.getText().toString();
                UserDetails.cMessage = messageText;
                if (!messageText.equals("")) {

                    String currentRegDate = System.currentTimeMillis()+"";

                    map.put("message", messageText);
                    map.put("user", UserDetails.cNickname);
                    map.put("regDate", currentRegDate);

                    map1.put("message", messageText);
                    map1.put("user", UserDetails.cNickname);
                    map1.put("with", UserDetails.cChatWith);
                    map1.put("regDate", currentRegDate);

                    map2.put("message", messageText);
                    map2.put("user", UserDetails.cChatWith);
                    map2.put("with", UserDetails.cNickname);
                    map2.put("regDate", currentRegDate);

                    mReference1.push().setValue(map);
                    mReference2.push().setValue(map);
                    mReference3.setValue(map1);
                    mReference4.setValue(map2);
                    mMessageArea.setText("");
                }
            }
        });

        mReference1.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Map map = dataSnapshot.getValue(Map.class);
                String message = map.get("message").toString();
                String id = map.get("user").toString();
                String regDate = map.get("regDate") == null ? "" : map.get("regDate").toString();

                Log.v("chatSystem", "메시지 = " + message);
                Log.v("chatSystem", "이름 = " + id);


                String date = Utils.getInst().convertMillisecond(regDate);


                /* 현재 날짜(-------yyyy.MM.dd--------) 구성하기 위함 */
                boolean isCheck = false;
                if(!prevDate.equals(date)){
                    isCheck = true;
                }
                prevDate = date;



                if(isCheck){
                    cAdapter.add("", regDate, 2);
                }

                if (id.equals(UserDetails.cNickname)) {
                    cAdapter.add(message, regDate, 1);
                } else {
                    cAdapter.add(message, regDate, 0);
                }
                cAdapter.notifyDataSetChanged();




            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }


}
