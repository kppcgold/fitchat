/*
 * Create by FitChat on 2018. 10. 1.
 * Copyright (c) 2018. FitChat. All rights reserved.
 *
 */

//TODO 유저 목록 리스트 아이템
package android.myproject.trams.fitchat.main;

import android.graphics.drawable.Drawable;
import android.widget.TextView;

public class LIstViewItem {
    private Drawable mProfile_img;
    private String mMember_name;
    private String mRecent_history;

    public void setmMember_name(String mMember_name) {
        this.mMember_name = mMember_name;
    }

    public void setmProfile_img(Drawable mProfile_img) {
        this.mProfile_img = mProfile_img;
    }

    public void setmRecent_history(String mRecent_history) {
        this.mRecent_history = mRecent_history;
    }

    public Drawable getmProfile_img() {
        return mProfile_img;
    }

    public String getmMember_name() {
        return mMember_name;
    }

    public String getmRecent_history() {
        return mRecent_history;
    }
}
