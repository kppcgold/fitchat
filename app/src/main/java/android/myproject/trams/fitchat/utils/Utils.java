package android.myproject.trams.fitchat.utils;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.myproject.trams.fitchat.main.MainActivity;
import android.myproject.trams.fitchat.more.EditInterestActivity;
import android.support.annotation.NonNull;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class Utils {

    private static Utils gInstance;

    @NonNull
    public static Utils getInst() {
        if (gInstance == null) {
            synchronized (Utils.class) {
                gInstance = new Utils();
            }
        }

        return gInstance;
    }

    /**
     * 로그인시 이동되는 페이지
     * @param mAct
     * @return
     */
    public void moveInitPage(Activity mAct) {

        SharedPreferences pref = mAct.getSharedPreferences("isFirst", Activity.MODE_PRIVATE);
        boolean first = pref.getBoolean("isFirst", false);
        if(!first){
            Log.d("Is first Time?", "first");
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("isFirst",true);
            editor.apply();
            mAct.startActivity(new Intent(mAct, EditInterestActivity.class));
            mAct.finish();

        }else{
            Log.d("Is first Time?", "not first");
            mAct.startActivity(new Intent(mAct, MainActivity.class));
            mAct.finish();
        }
    }


    /**
     * 시간 문자열값 얻기.
     *
     * @param p_time 시간값.
     * @return 문자열표현.
     */
    public static String getTimeExpression(Date p_time) {
        SimpleDateFormat w_sdf = new SimpleDateFormat("hh:mm aa", Locale.US);
        return w_sdf.format(p_time).toLowerCase();
    }


    /**
     * date to Millisecond
     */
    public String convertMillisecond(String dateValue){

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(Long.parseLong(dateValue));
            return sdf.format(calendar.getTime());
        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }




}
